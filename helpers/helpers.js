const MongoObject = require('mongoose').Types.ObjectId;
require('custom-env').env('dev');

exports.checkIfIdIsObject = id => {
  if(!MongoObject.isValid(id)){
    const err = new Error('The id is not an ObjectId!');
    err.status = 400;
    throw err;
  }
  return 'validation pased!'; //this return is just for testing purposes
}

exports.checkIfExist = obj => {
  if(!obj){
    const err = new Error('Failed attempt to execute request!');
    err.status = 500;
    throw err;
  }
  return 'validation pased!'; //this return is just for testing purposes
}

exports.cookieTokenExtractor = req =>{
    let token = null;
    if (req && req.cookies)
    {
      token = req.cookies['auth'];
    }
    console.log('token', token);
    return token;
};

exports.checkIfAuthenticated = (req, res, next) =>{
  console.log('req.session.passport.user', req.session.passport.user);
  if (req.isAuthenticated()) { return next(); }
  res.status(401).send('Not authorised');
}

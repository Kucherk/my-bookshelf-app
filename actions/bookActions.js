const Book        = require('../models/Book');
const { checkIfIdIsObject, checkIfExist } = require('../helpers/helpers');

exports.getBooksList = async (req, res) => {
  const list = await Book.find();
  if ( list.length < 1 ){
    const err = new Error('The library is empty! :(');
    err.status = 404;
    throw err; 
  }
  res.status(200).json(list);
}

exports.getBook = async (req, res) =>{
  await checkIfIdIsObject(req.params.id);
  const searchedBook = await Book.findById(req.params.id);
  if(!searchedBook){
    const err = new Error('There are no such a book!');
    err.status = 404;
    throw err;
  }
  res.status(200).json(searchedBook);
}

exports.addBook = async (req, res) => {
  let { title, author, year, janre } = req.body;
  if(!(title, author, year, janre)){
    const err = new Error('All fields must be filled!');
    err.status = 400;
    throw err;
  }
  const existingBook = await Book.findOne({title});
  if(existingBook){
    const err = new Error('Book with a same name is already exist!');
    err.status = 400;
    throw err;
  }
  const newBook = new Book({title, author, year, janre});
  const createdBook = await newBook.save();
  await checkIfExist(createdBook);
  res.status(200).json(createdBook);
}

exports.editBook = async (req, res) =>{
  await checkIfIdIsObject(req.params.id);
  let { title, author, year, janre } = req.body;
  const updateFields = {};
  if(title){ updateFields.title   = title};
  if(author){ updateFields.author = author};
  if(year){ updateFields.year     = year};
  if(janre){ updateFields.janre   = janre};

  const editedBook = await Book.findByIdAndUpdate(req.params.id, updateFields, {new: true});
  
  await checkIfExist(editedBook);
  res.status(200).json(editedBook);
}

exports.deleteBook = async (req, res) =>{
  await checkIfIdIsObject(req.params.id);
  const deletedBook = await Book.findByIdAndDelete(req.params.id);
  checkIfExist(deletedBook);
  res.status(200).send(`Book "${deletedBook.title}" was successfully deleted!`);
}



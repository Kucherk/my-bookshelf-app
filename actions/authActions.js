const jwt = require('jsonwebtoken');

const User = require('../models/User');

exports.loginUser = async (req, res) =>{
  console.log('req.body', req.body);
  const{name, password} = req.body;
  if(!(name, password)){
    const err = new Error('All fields must be filled!');
    err.status = 400;
    throw err;
  }
  const searchedUser = await User.findOne({name, password});
  if(!searchedUser){
    const err = new Error('Name or password are incorrect');
    err.status = 404;
    throw err;
  }

  const token = await searchedUser.generateJWT();
  await res.cookie('auth', token);
  res.redirect('/');
  //res.status(200).send(`Authentication successfull! Your token: ${token}`);
}


exports.logoutUser = (req, res) => {
  req.logout();
  res.clearCookie('auth');
  res.status(200).send('You\'ve been logout');
};


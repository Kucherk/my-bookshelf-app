const jwt = require('jsonwebtoken');

const User = require('../models/User');
const { checkIfIdIsObject, checkIfExist } = require('../helpers/helpers');

exports.getUsersList = async (req, res) => {
  const list = await User.find();
  if ( list.length < 1 ){
    const err = new Error('The list of users is empty! :(');
    err.status = 404;
    throw err; 
  }
  res.status(200).json(list);
}

exports.getUser = async (req, res) =>{
  await checkIfIdIsObject(req.params.id);
  const searchedUser = await User.findById(req.params.id);
  if(!searchedUser){
    const err = new Error('There are no such a user!');
    err.status = 404;
    throw err;
  }
  res.status(200).json(searchedUser);
}

exports.addUser = async (req, res) => {
  let { name, password } = req.body;
  if(!(name, password)){
    const err = new Error('All fields must be filled!');
    err.status = 400;
    throw err;
  }
  const existingUser = await User.findOne({name});
  if(existingUser){
    const err = new Error('User with a same name is already exist!');
    err.status = 400;
    throw err;
  }
  const newUser = new User({name, password});
  const createdUser = await newUser.save();
  await checkIfExist(createdUser);

  const token = await createdUser.generateJWT();

  res.status(200).json({user: createdUser, token});
}

exports.editUser = async (req, res) =>{
  await checkIfIdIsObject(req.params.id);
  let { name, password } = req.body;
  const updateFields = {};
  if(name){ updateFields.name         = name};
  if(password){ updateFields.password = password};

  const editedUser = await User.findByIdAndUpdate(req.params.id, updateFields, {new: true});
  
  await checkIfExist(editedUser);
  res.status(200).json(editedUser);
}

exports.deleteUser = async (req, res) =>{
  await checkIfIdIsObject(req.params.id);
  const deletedUser = await User.findByIdAndDelete(req.params.id);
  checkIfExist(deletedUser);
  res.status(200).send(`User "${deletedUser.name}" was successfully deleted!`);
}
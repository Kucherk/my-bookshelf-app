const express       = require('express');
const passport      = require('passport');
const multer        = require('multer');
const upload        = multer();
const User          = require('../models/User');
const router        = express.Router(); 
const {catchErrors} = require('../errors/errorHandler');
const {getUsersList, getUser, addUser, editUser, deleteUser} = require('../actions/userActions');

router
  .route('/user')
  .get(catchErrors(getUsersList))
  .post(upload.none(), catchErrors(addUser))
router
  .route('/user/:id')
  .get(catchErrors(getUser))
  .put(upload.none(), catchErrors(editUser))
  .delete(catchErrors(deleteUser));
/*router
  .route('/newUser')
  .post((req, res)=>{
    const {email, password} = req.body;

    if(!(email, password)){
      const err = new Error('All fields must be filled!');
      err.status = 400;
      throw err;
    }
    const user = new User({email, password});
    user.setPassword(password);
    const newUser = user.save();

    res.status(200).json({ user: newUser.toAuthJSON() });
  });

router
  .route('/loginUser')
  .post((req, res)=>{
    const {email, password} = req.body;

    if(!(email, password)){
      const err = new Error('All fields must be filled!');
      err.status = 400;
      throw err;
    }
    passport.authenticate('local', { session: false }, (err, passportUser, info) => {
      if(err) {
        throw err;
      }

      if(passportUser) {
        const user = passportUser;
        user.token = passportUser.generateJWT();

        return res.status(200).json({ user: user.toAuthJSON() });
      }
      return status(400).info;
    });
  });*/

  module.exports = router;
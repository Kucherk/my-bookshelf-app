const express       = require('express');
const multer        = require('multer');
const passport      = require('passport');
const upload        = multer();
const router        = express.Router(); 
const { loginUser, logoutUser } = require('../actions/authActions');
const {catchErrors} = require('../errors/errorHandler');

router
  .route('/login')
    .post(upload.none(), catchErrors(loginUser));
router
  .route('/logout')
    .get(catchErrors(logoutUser));
router
  .route('/login/goodreads')
    .get(passport.authenticate('goodreads'));
router
  .route('/login/goodreads/callback')
    .get(passport.authenticate('goodreads',{ session: true }), (req, res)=>{
      res.redirect('/');
    });
router
  .route('/login/facebook')
    .get(passport.authenticate('facebook',{ session: true }));
router
  .route('/login/facebook/callback')
    .get(passport.authenticate('facebook',{ session: true }), (req, res)=>{
      res.redirect('/');
    });
module.exports = router;
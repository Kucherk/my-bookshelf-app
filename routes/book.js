const express       = require('express');
const multer        = require('multer');
const passport      = require('passport');
const passportConf  = require('../modules/passport');
const router        = express.Router(); 
const upload        = multer();
const {getBooksList, 
       getBook, 
       addBook, 
       editBook, 
       deleteBook}   = require('../actions/bookActions');
const {catchErrors}  = require('../errors/errorHandler');
const {checkIfAuthenticated} = require('../helpers/helpers')
router
  .route('/library')
  .get( checkIfAuthenticated, 
        catchErrors(getBooksList))
  .post(upload.none(),
        checkIfAuthenticated,
        catchErrors(addBook));
router
  .route('/library/:id')
  .get(passport.authenticate('jwt', { session: true }), 
       catchErrors(getBook))
  .put(upload.none(), 
       checkIfAuthenticated,
       catchErrors(editBook))
  .delete(catchErrors(deleteBook));


module.exports = router;
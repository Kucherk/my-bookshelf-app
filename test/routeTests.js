const request = require('supertest');
const expect  = require('chai').expect;
const Book    = require('../models/Book');
const {app}   = require('../index');
const { checkIfIdIsObject, checkIfExist } = require('../helpers/helpers');

describe('booklist page', () =>{
  it('showes the whole library', (done)=>{
    request(app).get('/library')
    .expect('Content-Type', /json/)
    .expect(200)
    .end( (err, res) =>{
      if(err){
        return done(err);
      }
      done();
    });
  });

  it('add book to the library', (done)=>{
    request(app).post('/library')
    .send({title: "test title", author:"test author", year: "10000", janre:"test janre"})
    .expect('Content-Type', /json/)
    .expect(200)
    .end( async (err, res) =>{
      if(err){
        return done(err);
      }
      await Book.findByIdAndDelete(res.body._id);
      done();
    });
  });

  it('get book by id', (done) =>{
    const book = new Book({title: "test title", author:"test author", year: "10000", janre:"test janre"});
    book.save( (err, testBook) =>{
      request(app).get(`/library/${testBook._id}`)
      .expect('Content-Type', /json/)
      .expect(200)
      .end( async (err, res) =>{
        if(err){
          return done(err);
        }
        await Book.findByIdAndDelete(res.body._id);
        done();
      });
    });
  });

  it('edit book data', (done) =>{
    const book = new Book({title: "test title", author:"test author", year: "10000", janre:"test janre"});
    book.save( (err, testBook) =>{
      request(app).put(`/library/${testBook._id}`)
      .send({title: "test title2", author:"test author2"})
      .expect('Content-Type', /json/)
      .expect(200)
      .end( async (err, res) =>{
        if(err){
          return done(err);
        }
        await Book.findByIdAndDelete(res.body._id);
        done();
      });
    });
  });

  it('delete book', (done) =>{
    const book = new Book({title: "test title", author:"test author", year: "10000", janre:"test janre"});
    book.save( (err, testBook) =>{
      request(app).delete(`/library/${testBook._id}`)
      .expect('Content-Type', /text/)
      .expect(200)
      .expect(/was successfully deleted/, done);
    });
  });
});

describe('helper functions', () =>{
  it('check if given id is an object', (done) =>{
    expect(checkIfIdIsObject('5d22f8b427d289199127c71b')).to.be.a('string');
    expect(checkIfIdIsObject('5d22f8b427d289199127c71b')).to.equal('validation pased!');
    expect(checkIfIdIsObject).to.throw();
    done();
  });
  it('check if parameter is exist', (done) =>{
    expect(checkIfExist({test:'test'})).to.be.a('string');
    expect(checkIfExist({test:'test'})).to.equal('validation pased!');
    expect(checkIfExist).to.throw();
    done();
  }); 
})
const express      = require('express');
const bodyParser   = require('body-parser');
const cookieParser = require('cookie-parser');
const session      = require('express-session');
const passport     = require('passport');
require('custom-env').env('dev');
//const cookieSession= require ('cookie-session');
const app          = express();
app.use(session({
  secret: process.env.JWTSECRET,
  resave: false,
  saveUninitialized: true,
  cookie: {maxAge: 24 *60 *60 * 1000}}));
app.use(passport.initialize());
app.use(passport.session());
const bookRouter   = require('./routes/book');
const userRouter   = require('./routes/user');
const authRouter   = require('./routes/auth');
const {CustomErrors, notFoundError, dbValidationErrors} = require('./errors/errorHandler');

require('custom-env').env('dev');
require('./modules/mongo');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use(cookieParser());

app.use(bookRouter);
app.use(userRouter);
app.use(authRouter);

app.get('/', (req, res) =>{
    return res.status(200).send('It\'s a main page. If you get here, you probably autorized', );
});

app.use(notFoundError);
app.use(dbValidationErrors);
app.use(CustomErrors);

app.listen(process.env.PORT || 3000, () =>{
  console.log(`Connection is successful! We are listening port ${process.env.PORT}`);
});

module.exports.app = app;
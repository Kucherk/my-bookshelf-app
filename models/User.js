const mongoose     = require('mongoose');
const findOrCreate = require('mongoose-findorcreate');
const crypto       = require('crypto');
const jwt          = require('jsonwebtoken');
require('custom-env').env('dev');

const UserSchema = new mongoose.Schema({
  name: {type: String, required: true, unique: true},
  password: {type: String, required: true},
  goodreadsId: {type: Number, required: false},
  facebookId: {type: Number, required: false}
});

UserSchema.methods.setPassword = function(password) {
  this.salt = crypto.randomBytes(16).toString('hex');
  this.hash = crypto.pbkdf2Sync(password, this.salt, 10000, 512, 'sha512').toString('hex');
};

UserSchema.methods.validatePassword = function(password) {
  const hash = crypto.pbkdf2Sync(password, this.salt, 10000, 512, 'sha512').toString('hex');
  return this.hash === hash;
};

UserSchema.methods.generateJWT = function() {
  return jwt.sign({
    iat: new Date().getTime(),
    exp: new Date().setDate(new Date().getDate() + 1),
    id: this._id,
    name: this.name,
    password: this.password,
    goodreads: this.goodreadsId,
    facebookId: this.facebookId
  }, process.env.JWTSECRET);
}

UserSchema.methods.toAuthJSON = function() {
  return {
    _id: this._id,
    email: this.email,
    token: this.generateJWT(),
  };
};
UserSchema.plugin(findOrCreate);
const User = mongoose.model('User', UserSchema);
module.exports = User;
const mongoose   = require('mongoose');
const BookSchema = new mongoose.Schema({
  title:   { type: String, required: true, unique: true },
  author:  { type: String, required: true },
  year:    { type: Number, required: true },
  janre:   { type: String, required: true },
  rating:  { type: Number, required: true, default: 0 },
  opinion: [
    {
      mark: { type: Number, min: 1, max: 10, required: true },
      post: { type: String }
    }
  ]
});

const Book = mongoose.model('Book', BookSchema);

module.exports = Book;
const passport          = require('passport');
const JWTStrategy       = require('passport-jwt').Strategy;
const GoodreadsStrategy = require('passport-goodreads').Strategy;
const FacebookStrategy  = require('passport-facebook').Strategy;
const User              = require('../models/User');
const { cookieTokenExtractor } = require('../helpers/helpers');
require('custom-env').env('dev');

passport.serializeUser((user,done)=>{
  console.log(`We are going to serialize this: ${user.id}`);
  done(null,user.id);
});
passport.deserializeUser((id,done)=>{
  console.log(`We are going to deserialize this: ${id}`);
  User.findById(id)
  .then( user =>{
    console.log('foundUser', user);
    done(null,user);
  });
});

passport.use(new JWTStrategy({
  jwtFromRequest: cookieTokenExtractor,
  secretOrKey: process.env.JWTSECRET
}, async (payload, done) => {
  User.findById(payload.id)
    .then((user) => {
      if(!user) {
        return done(null, false, { errors: { 'email or password': 'is invalid' } });
      }

      return done(null, user);
    }).catch(done);
}));

passport.use(new GoodreadsStrategy({
    consumerKey: process.env.GOODREADS_KEY,
    consumerSecret: process.env.GOODREADS_SECRET,
    callbackURL: "/login/goodreads/callback"
  },
  async function(token, tokenSecret, profile, done) {
    await User.findOrCreate({ goodreadsId: profile.id, 
                        name: profile.displayName,
                        password: profile.id}, (err, user)=>{
                          done(null, user);
                        });
  }
));

passport.use(new FacebookStrategy({
    clientID: process.env.FACEBOOK_APP_ID,
    clientSecret: process.env.FACEBOOK_APP_SECRET,
    callbackURL: "/login/facebook/callback"
  },
  function(accessToken, refreshToken, profile, done) {
    User.findOrCreate({ facebookId: profile.id,
                        name: profile.displayName,
                        password: profile.id}, function (err, user) {
      return done(err, user);
    });
  }
));
const mongoose = require('mongoose');
mongoose.connect(process.env.DATABASE, {useNewUrlParser: true})
.then( () =>{
  console.log('We are succesfully connected to database');
} )
.catch( err =>{
  throw err;
});
mongoose.Promise = global.Promise;